import UIKit
import GoogleMaps
import SwiftyJSON

class HomeController : UIViewController, GMSMapViewDelegate, SidebarControllerDelegate {
    @IBOutlet weak var mapView: GMSMapView!
    var sidebarController: SidebarController?
    var locationAvailable: Bool = false

    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .default
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        navigationItem.leftBarButtonItem = UIBarButtonItem(image: UIImage(named: "ic_burger_menu.png"), style: .plain, target: self, action: #selector(btnSidebarClicked))
        
        let logoContainer = UIView(frame: CGRect(x: 0, y: 0, width: 120, height: 28))
        let imageView = UIImageView(frame: CGRect(x: 0, y: 0, width: 120, height: 28))
        imageView.contentMode = .scaleAspectFit
        imageView.image = UIImage(named: "ic_nav_milov.png")
        logoContainer.addSubview(imageView)
        
        navigationItem.titleView = logoContainer
        navigationItem.rightBarButtonItem = UIBarButtonItem(image: UIImage(named: "ic_notification.png"), style: .plain, target: self, action: #selector(btnNotificationClicked))
        
        self.mapView.isMyLocationEnabled = true
        self.mapView.settings.myLocationButton = true
        self.mapView.settings.compassButton = true
        
        if let user = UserDefaults.standard.string(forKey: "user") {
            print("user: \(user)")
        }                
    }
    
    @objc func didUpdateLocationsNotification(notification: Notification) {
        if locationAvailable == false {
            locationAvailable = true
            let location = CLLocation(latitude: locationLatitude, longitude: locationLongitude)
            mapView.camera = GMSCameraPosition(target: location.coordinate, zoom: 15, bearing: 0, viewingAngle: 0)
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.isNavigationBarHidden = false
        NotificationCenter.default.addObserver(self, selector: #selector(self.didUpdateLocationsNotification), name: Notification.Name("didUpdateLocations"), object: nil)
    }

    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.navigationController?.isNavigationBarHidden = true
        NotificationCenter.default.removeObserver(self, name: Notification.Name("didUpdateLocations"), object: nil)
    }

    @IBAction func btnSidebarClicked(sender: UIButton?) {
        self.sidebarController = SidebarController()
        self.sidebarController?.delegate = self
        sidebarController?.show()
    }

    func sidebarDidHide() {
        self.sidebarController = nil
    }
    
    @IBAction func btnNotificationClicked(sender: UIButton?) {
    }

    @IBAction func btnHomeClicked(sender: UIButton?) {
        let controller = HomeController()
        self.navigationController?.viewControllers = [controller]
    }

    @IBAction func btnExploreClicked(sender: UIButton?) {
    }

    @IBAction func btnScanClicked(sender: UIButton?) {
    }

    @IBAction func btnMatchedClicked(sender: UIButton?) {
    }

    @IBAction func btnYouClicked(sender: UIButton?) {
        let controller = ProfileController()
        self.navigationController?.viewControllers = [controller]
    }

}
