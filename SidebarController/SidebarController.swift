import UIKit

protocol SidebarControllerDelegate : class {
    func sidebarDidHide()
}

class SidebarController : UIViewController {
    weak var delegate: SidebarControllerDelegate?
    @IBOutlet weak var container: UIView!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var labelProfile: UILabel!
    var oldLeftBarButtonItem: UIBarButtonItem? = nil
    var oldRightBarButtonItem: UIBarButtonItem? = nil

    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.tableView.register(UINib(nibName: SidebarCell.className(), bundle: nil), forCellReuseIdentifier: SidebarCell.className())
    }

    @IBAction func btnCloseClicked(sender: UIButton!) {
        hide {
        }
    }
    
    func show() {
        let app = UIApplication.shared.delegate as! AppDelegate
        let navigationController: UINavigationController = app.window?.rootViewController as! UINavigationController
        oldLeftBarButtonItem = navigationController.topViewController?.navigationItem.leftBarButtonItem
        oldRightBarButtonItem = navigationController.topViewController?.navigationItem.rightBarButtonItem
        navigationController.topViewController?.navigationItem.leftBarButtonItem = UIBarButtonItem(image: UIImage(named: "ic_back_arrow.png"), style: .plain, target: self, action: #selector(btnCloseClicked))
        navigationController.topViewController?.navigationItem.rightBarButtonItem = nil
        let parent = navigationController.topViewController?.view;
        self.view.frame = CGRect.init(x: 0.0, y: 0.0, width: (parent?.frame.size.width)!, height: (parent?.frame.size.height)!);
        self.view.backgroundColor = UIColor.init(red: 0.0, green: 0.0, blue: 0.0, alpha: 0.0);
        let r = self.container.frame;
        self.container.frame = CGRect.init (x: -r.size.width, y: 0.0, width: r.size.width, height: r.size.height);
        parent?.addSubview(self.view)
        UIView.animate(withDuration: 0.3, animations: {
            self.view.backgroundColor = UIColor.init(red: 0.0, green: 0.0, blue: 0.0, alpha: 0.5)
            let r = self.container.frame;
            self.container.frame = CGRect.init (x: 0.0, y: 0.0, width: r.size.width, height: r.size.height);
        }) { (finished) in
        }
    }
    
    func hide(completion: @escaping () -> Void) {
        UIView.animate(withDuration: 0.3, animations: {
            self.view.backgroundColor = UIColor.init(red: 0.0, green: 0.0, blue: 0.0, alpha: 0.0);
            let r = self.container.frame;
            self.container.frame = CGRect.init (x: -r.size.width, y: 0.0, width: r.size.width, height: r.size.height);
        }) { (finished) in
            let app = UIApplication.shared.delegate as! AppDelegate
            let navigationController: UINavigationController = app.window?.rootViewController as! UINavigationController
            navigationController.topViewController?.navigationItem.leftBarButtonItem = self.oldLeftBarButtonItem
            navigationController.topViewController?.navigationItem.rightBarButtonItem = self.oldRightBarButtonItem
            (completion)()
            self.view.removeFromSuperview()
            self.delegate?.sidebarDidHide()
        }
    }
    
    @IBAction func btnSignOutClicked(sender: UIButton!) {
        hide {
        }
    }

}

extension SidebarController : UITableViewDataSource, UITableViewDelegate {
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 7
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return SidebarCell.rowHeight()
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        switch indexPath.row {
        case 0:
            let cell = tableView.dequeueReusableCell(withIdentifier: SidebarCell.className(), for: indexPath) as! SidebarCell
            cell.iv.image = UIImage(named: "ic_usage.png")
            cell.labelName.text = NSLocalizedString("Cara Pakai", comment: "")
            return cell
        case 1:
            let cell = tableView.dequeueReusableCell(withIdentifier: SidebarCell.className(), for: indexPath) as! SidebarCell
            cell.iv.image = UIImage(named: "ic_qa.png")
            cell.labelName.text = NSLocalizedString("Tanya Jawab", comment: "")
            return cell
        case 2:
            let cell = tableView.dequeueReusableCell(withIdentifier: SidebarCell.className(), for: indexPath) as! SidebarCell
            cell.iv.image = UIImage(named: "ic_contact_us.png")
            cell.labelName.text = NSLocalizedString("Hubungi Kami", comment: "")
            return cell
        case 3:
            let cell = tableView.dequeueReusableCell(withIdentifier: SidebarCell.className(), for: indexPath) as! SidebarCell
            cell.iv.image = UIImage(named: "ic_about.png")
            cell.labelName.text = NSLocalizedString("Tentang MILOV", comment: "")
            return cell
        case 4:
            let cell = tableView.dequeueReusableCell(withIdentifier: SidebarCell.className(), for: indexPath) as! SidebarCell
            cell.iv.image = UIImage(named: "ic_terms.png")
            cell.labelName.text = NSLocalizedString("Syarat & Ketentuan", comment: "")
            return cell
        case 5:
            let cell = tableView.dequeueReusableCell(withIdentifier: SidebarCell.className(), for: indexPath) as! SidebarCell
            cell.iv.image = UIImage(named: "ic_privacy_policy.png")
            cell.labelName.text = NSLocalizedString("Privacy Policy", comment: "")
            return cell
        case 6:
            let cell = tableView.dequeueReusableCell(withIdentifier: SidebarCell.className(), for: indexPath) as! SidebarCell
            cell.iv.image = UIImage(named: "ic_settings.png")
            cell.labelName.text = NSLocalizedString("Settings", comment: "")
            return cell
        default:
            break
        }
        return UITableViewCell()
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath as IndexPath, animated: true)
        switch indexPath.row {
        case 0:
            hide {
            }
        case 1:
            hide {
            }
        case 2:
            hide {
            }
        case 3:
            hide {
            }
        case 4:
            hide {
            }
        case 5:
            hide {
            }
        case 6:
            hide {
            }
        default:
            break
        }
    }
}
