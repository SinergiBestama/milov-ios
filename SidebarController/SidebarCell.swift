import UIKit

class SidebarCell: UITableViewCell {
    @IBOutlet weak var iv: UIImageView!
    @IBOutlet weak var labelName: UILabel!

    static func rowHeight () -> CGFloat {
        return 40.0
    }
}
