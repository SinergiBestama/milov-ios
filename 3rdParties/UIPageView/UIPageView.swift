import UIKit

protocol UIPageViewDelegate: class {
    func pageViewDidSelect(pageView: UIPageView)
}

@IBDesignable class UIPageView: UIScrollView, UIScrollViewDelegate {
    weak var eventDelegate: UIPageViewDelegate? = nil
    var views: [UIView] = [] {
        didSet {
            if (views.count > 0) {
                for index in 0..<views.count {
                    let view = views[ index ]
                    addSubview(view)
                }
            }
            contentOffset = CGPoint(x: 0.0, y: 0.0)
            contentSize = CGSize(width: CGFloat(views.count) * frame.size.width, height: frame.size.height)
            layoutSubviews()
        }
    }
    
    func select(index: Int) {
        if (selected == index) {
            return
        }
        selected = index
        setContentOffset(CGPoint(x: CGFloat(index) * frame.size.width, y: 0.0), animated: true)
    }
    
    func getSelected() -> Int {
        return selected
    }

    public override init(frame: CGRect) {
        super.init(frame: frame)
        sharedInit()
    }
    
    required public init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        sharedInit()
    }

    private var selected = -1
    
    func sharedInit() {
        showsVerticalScrollIndicator = false
        showsHorizontalScrollIndicator = false
        clipsToBounds = true
        isPagingEnabled = true
        delegate = self
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        if (views.count > 0) {
            for index in 0..<views.count {
                let view = views[ index ]
                view.frame = CGRect(x: CGFloat(index) * frame.size.width, y: 0.0, width: frame.size.width, height: frame.size.height)
            }
        }
        contentSize = CGSize(width: CGFloat(views.count) * frame.size.width, height: frame.size.height)
    }
    
    
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        let index = lround (Double(contentOffset.x / frame.size.width))
        if (selected != index) {
            selected = index
            if let dele: UIPageViewDelegate = eventDelegate {
                dele.pageViewDidSelect(pageView: self)
            }
        }
    }

}

