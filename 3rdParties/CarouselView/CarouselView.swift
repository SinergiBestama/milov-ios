import UIKit

protocol CarouselViewDelegate: class {
    func carouselDidScrolled(carousel: CarouselView, index: Int)
    func carouselDidTapped(carousel: CarouselView, index: Int)
}

@IBDesignable class CarouselView: UIView, UIScrollViewDelegate {
    var scrollView: UIScrollView?
    var eventDelegate: CarouselViewDelegate? = nil
    var images: [UIImage?] = [] {
        didSet {
            var x: CGFloat = 0.0
            for index in 0..<self.slides.count {
                let view = self.slides[ index ]
                view.frame = CGRect(x: x, y: 0.0, width: frame.size.width, height: frame.size.height)
                self.scrollView?.addSubview(view)
                x += frame.size.width
            }
            
            self.pageControl?.numberOfPages = images.count
            self.pageControl?.currentPage = 0

            if (images.count > 0) {
                select(index: 0)
            }
            else {
                select(index: -1)
            }
            
            if (self.timer != nil) {
                self.timer?.invalidate()
                self.timer = nil
            }
            if (images.count > 0) {
                self.timer = Timer.scheduledTimer(timeInterval: 3.5, target: self, selector: #selector(timerAction), userInfo: nil, repeats: true)
            }
        }
    }
    
    @objc func timerAction() {
        self.scrollView?.setContentOffset(CGPoint(x: frame.size.width * 2.0, y: 0.0), animated: true)
    }
    
    func stop() {
        if (self.timer != nil) {
            self.timer?.invalidate()
            self.timer = nil
        }
    }
    
    private var timer: Timer? = nil
    
    override func layoutSubviews() {
        super.layoutSubviews()
        var x: CGFloat = 0.0
        for index in 0..<self.slides.count {
            let view = self.slides[ index ]
            view.frame = CGRect(x: x, y: 0.0, width: frame.size.width, height: frame.size.height)
            x += frame.size.width
        }
        self.scrollView?.frame = CGRect(x: 0.0, y: 0.0, width: frame.size.width, height: frame.size.height)
        self.scrollView?.contentSize = CGSize(width: CGFloat(self.slides.count) * frame.size.width, height: frame.size.height)
        self.scrollView?.scrollRectToVisible(CGRect(x: frame.size.width, y: 0.0, width: frame.size.width, height: frame.size.height), animated: false);
    }
    
    private var pageControl: UIPageControl? = nil
    
    private var selected = -1
    func select(index: Int) {
        selected = index
        if (self.images.count == 0) {
            return
        }
        var left: Int = -1
        if (selected > 0) {
            left = selected-1
        }
        else {
            left = images.count-1
        }
        var right: Int = -1
        if (selected < (images.count-1)) {
            right = selected+1
        }
        else {
            right = 0
        }
        
        // left
        for subview in self.slides[ 0 ].subviews {
            subview.removeFromSuperview()
        }
        self.slides[ 0 ].image = self.images[ left ]
        
        // selected
        for subview in self.slides[ 1 ].subviews {
            subview.removeFromSuperview()
        }
        self.slides[ 1 ].image = self.images[ selected ]
        
        // right
        for subview in self.slides[ 2 ].subviews {
            subview.removeFromSuperview()
        }
        self.slides[ 2 ].image = self.images[ right ]
        
        self.scrollView?.scrollRectToVisible(CGRect(x: frame.size.width, y: 0.0, width: frame.size.width, height: frame.size.height), animated: false);
        
        pageControl?.currentPage = selected
        
        if let dele: CarouselViewDelegate = eventDelegate {
            dele.carouselDidScrolled(carousel: self, index: selected)
        }

    }
    
    public override init(frame: CGRect) {
        super.init(frame: frame)
        sharedInit()
    }
    
    required public init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        sharedInit()
    }
    
    private var slides: [UIImageView] = []
    func sharedInit() {
        self.scrollView = UIScrollView()
        self.scrollView?.frame = CGRect(x: 0.0, y: 0.0, width: self.frame.size.width, height: self.frame.size.height)
        self.scrollView?.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        self.scrollView?.showsVerticalScrollIndicator = false
        self.scrollView?.showsHorizontalScrollIndicator = false
        self.scrollView?.clipsToBounds = true
        self.scrollView?.isPagingEnabled = true
        self.scrollView?.delegate = self
        addSubview(self.scrollView!)
        self.slides = [UIImageView(), UIImageView(), UIImageView()]
        
        self.pageControl = UIPageControl()
        self.pageControl?.frame = CGRect(x: 0.0, y: frame.size.height - 50.0, width: frame.size.width, height: 50.0)
        self.pageControl?.autoresizingMask = [.flexibleWidth, .flexibleTopMargin]
        self.pageControl?.isUserInteractionEnabled = false
        addSubview(pageControl!)
        bringSubviewToFront(self.pageControl!)

        let singleTap = UITapGestureRecognizer(target: self, action: #selector(handleTap(_:)))
        singleTap.cancelsTouchesInView = false
        singleTap.numberOfTapsRequired = 1
        addGestureRecognizer(singleTap)
    }
    
    @objc func handleTap(_ recognizer: UITapGestureRecognizer) {
        if let dele: CarouselViewDelegate = eventDelegate {
            dele.carouselDidTapped(carousel: self, index: selected)
        }
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
    }
    
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        print("scrollViewDidEndDecelerating")
        if(scrollView.contentOffset.x > scrollView.frame.size.width) {
            if (selected >= (self.images.count-1)) {
                select(index: 0)
            }
            else {
                select(index: selected+1)
            }
        }
        if(scrollView.contentOffset.x < scrollView.frame.size.width) {
            if (selected > 0) {
                select(index: selected-1)
            }
            else {
                select(index: (self.images.count-1))
            }
        }
    }
    
    func scrollViewDidEndScrollingAnimation(_ scrollView: UIScrollView) {
        if (selected >= (self.images.count-1)) {
            select(index: 0)
        }
        else {
            select(index: selected+1)
        }
    }
    
    deinit {
        if (self.timer != nil) {
            self.timer?.invalidate()
            self.timer = nil
        }
    }
}

