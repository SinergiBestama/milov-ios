import UIKit
import Alamofire
import AlamofireImage
import SwiftyJSON

var activityIndicatorCount: Int = 0

func POST(url: String, params: [String:Any]? = nil, headers: [String:Any]? = nil, encoding: ParameterEncoding = JSONEncoding.default, cached: Bool = false, completion: @escaping (_ success: Bool, _ json: JSON) -> Void) {
    activityIndicatorCount = activityIndicatorCount + 1
    UIApplication.shared.isNetworkActivityIndicatorVisible = true
    
    var cacheKey: String = url
    if cached == true {
        if params != nil {
            cacheKey = cacheKey + "?"
            for (key, value) in params! {
                cacheKey = cacheKey + "\(key)=\(value)&"
            }
        }
        if let json = NSCACHE[cacheKey] {
            let dict = JSON(json)
            completion(true, dict)
        }
        else {
            if let json = UserDefaults.standard.string(forKey: cacheKey) {
                let dict = json.json()
                completion(true, dict)
            }
        }
    }
    
    Alamofire.request(url, method: .post, parameters: params, encoding: encoding, headers: headers as? HTTPHeaders).responseJSON { response in
        activityIndicatorCount = activityIndicatorCount - 1
        if (activityIndicatorCount <= 0) {
            UIApplication.shared.isNetworkActivityIndicatorVisible = false
        }
        switch response.result {
        case .success(let json):
            if cached == true {
                NSCACHE[cacheKey] = json as AnyObject
                let dict = JSON(json)
                UserDefaults.standard.set(dict.string(), forKey: cacheKey)
                UserDefaults.standard.synchronize()
            }
            let dict = JSON(json)
            completion(true, dict)
            break
        case .failure( _):
            completion(false, JSON.null)
            break
        }
    }
}

func GET(url: String, params: [String:Any]? = nil, headers: [String:Any]? = nil, cached: Bool = false, completion: @escaping (_ success: Bool, _ json: JSON) -> Void) {
    activityIndicatorCount = activityIndicatorCount + 1
    UIApplication.shared.isNetworkActivityIndicatorVisible = true
    
    var cacheKey: String = url
    if cached == true {
        if params != nil {
            cacheKey = cacheKey + "?"
            for (key, value) in params! {
                cacheKey = cacheKey + "\(key)=\(value)&"
            }
        }
        if let json = NSCACHE[cacheKey] {
            let dict = JSON(json)
            completion(true, dict)
        }
        else {
            if let json = UserDefaults.standard.string(forKey: cacheKey) {
                let dict = json.json()
                completion(true, dict)
            }
        }
    }

    Alamofire.request(url, method: .get, parameters: params, headers: headers as? HTTPHeaders).responseJSON { response in
        activityIndicatorCount = activityIndicatorCount - 1
        if (activityIndicatorCount <= 0) {
            UIApplication.shared.isNetworkActivityIndicatorVisible = false
        }
        switch response.result {
        case .success(let json):
            if cached == true {
                NSCACHE[cacheKey] = json as AnyObject
                let dict = JSON(json)
                UserDefaults.standard.set(dict.string(), forKey: cacheKey)
                UserDefaults.standard.synchronize()
            }
            let dict = JSON(json)
            completion(true, dict)
            break
        case .failure( _):
            completion(false, JSON.null)
            break
        }
    }
}

func DEL(url: String, params: [String:Any]? = nil, headers: [String:Any]? = nil, completion: @escaping (_ success: Bool, _ json: JSON) -> Void) {
    activityIndicatorCount = activityIndicatorCount + 1
    UIApplication.shared.isNetworkActivityIndicatorVisible = true
    request(url, method: .delete, parameters: params, headers: headers as? HTTPHeaders).responseJSON { response in
        activityIndicatorCount = activityIndicatorCount - 1
        if (activityIndicatorCount <= 0) {
            UIApplication.shared.isNetworkActivityIndicatorVisible = false
        }
        switch response.result {
        case .success(let json):
            let dict = JSON(json)
            completion(true, dict)
            break
        case .failure( _):
            completion(false, JSON.null)
            break
        }
    }
}

func PUT(url: String, params: [String:Any]? = nil, headers: [String:Any]? = nil, completion: @escaping (_ success: Bool, _ json: JSON) -> Void) {
    activityIndicatorCount = activityIndicatorCount + 1
    UIApplication.shared.isNetworkActivityIndicatorVisible = true
    request(url, method: .put, parameters: params, encoding: JSONEncoding.default, headers: headers as? HTTPHeaders).responseJSON { response in
        activityIndicatorCount = activityIndicatorCount - 1
        if (activityIndicatorCount <= 0) {
            UIApplication.shared.isNetworkActivityIndicatorVisible = false
        }
        switch response.result {
        case .success(let json):
            let dict = JSON(json)
            completion(true, dict)
            break
        case .failure( _):
            completion(false, JSON.null)
            break
        }
    }
}

func CACHE(url: String) {
    let imageDownloader = UIImageView.af_sharedImageDownloader
    imageDownloader.download(URLRequest(url: URL(string: url)!), completion: { _ in
    })
}

func CACHED(url: String) -> UIImage? {
    let imageDownloader = UIImageView.af_sharedImageDownloader
    return imageDownloader.imageCache?.image(for: URLRequest(url: URL(string: url)!), withIdentifier: nil)
}

func POSTRAW(url: String, params: [String:Any]? = nil, headers: [String:Any]? = nil, cached: Bool = false, completion: @escaping (_ success: Bool, _ raw: String) -> Void) {
    activityIndicatorCount = activityIndicatorCount + 1
    UIApplication.shared.isNetworkActivityIndicatorVisible = true
    
    var cacheKey: String = url
    if cached == true {
        if params != nil {
            cacheKey = cacheKey + "?"
            for (key, value) in params! {
                cacheKey = cacheKey + "\(key)=\(value)&"
            }
        }
        if let raw = NSCACHE[cacheKey] {
            completion(true, raw as! String)
        }
    }
    
    Alamofire.request(url, method: .post, parameters: params, headers: headers as? HTTPHeaders).response { response in
        activityIndicatorCount = activityIndicatorCount - 1
        if (activityIndicatorCount <= 0) {
            UIApplication.shared.isNetworkActivityIndicatorVisible = false
        }
        if let data = response.data, let utf8Text = String(data: data, encoding: .utf8) {
            print("Data: \(utf8Text)")
            if cached == true {
                NSCACHE[cacheKey] = utf8Text as AnyObject
            }
            completion(true, utf8Text)
        }
        else {
            if cached == true {
                NSCACHE[cacheKey] = "" as AnyObject
            }
            completion(false, "")
        }
    }
}


