import Foundation

var NSCACHE = SharedCache()

class SharedCache: NSObject {
    let storage = NSCache<AnyObject, AnyObject>()
    
    subscript(key: String) -> AnyObject? {
        get {
            return storage.object(forKey: key as AnyObject)
        }
        set {
            if let newValue = newValue {
                storage.setObject(newValue, forKey: key as AnyObject)
            }
            else {
                storage.removeObject(forKey: key as AnyObject)
            }
        }
    }
}


