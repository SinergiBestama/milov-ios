import UIKit

public enum UITabViewType : Int {
    case fixed
    case android
    case tabbed
    case rounded
}

protocol UITabViewDelegate : class {
    func tabViewDidSelect(tabView: UITabView, index: Int)
}

@IBDesignable class UITabView: UIScrollView {
    weak var eventDelegate: UITabViewDelegate?
    var type: UITabViewType = .fixed {
        didSet {
            reloadData()
        }
    }
    
    var items: [String] = [] {
        didSet {
            reloadData()
        }
    }

    var font: UIFont = UIFont.systemFont(ofSize: 14.0)
    @IBInspectable var btnColor: UIColor = .clear
    @IBInspectable var textColor: UIColor = .darkGray
    @IBInspectable var indiColor: UIColor = .clear
    var selFont: UIFont = UIFont.boldSystemFont(ofSize: 14.0)
    @IBInspectable var selBtnColor: UIColor = .green
    @IBInspectable var selTextColor: UIColor = .white
    @IBInspectable var selIndiColor: UIColor = .yellow
    @IBInspectable var bdrWidth: CGFloat = 1.0
    @IBInspectable var bdrColor: UIColor = .lightGray
    @IBInspectable var xPadding: CGFloat = 0.0
    @IBInspectable var indiHeight: CGFloat = 2.0
    
    func select(index: Int) {
        if (selected == index) {
            return
        }
        selected = index
        for btn in btns  {
            if (btn.tag == selected) {
                btn.backgroundColor = selBtnColor
                btn.titleLabel?.font = selFont
                btn.setTitleColor(selTextColor, for: .normal)
                if (type == .rounded) {
                    btn.layer.borderWidth = 0.0
                }
                scrollRectToVisible(btn.frame, animated: true)
                if let dele: UITabViewDelegate = eventDelegate {
                    dele.tabViewDidSelect(tabView: self, index: btn.tag)
                }
            }
            else {
                btn.backgroundColor = btnColor
                btn.titleLabel?.font = font
                btn.setTitleColor(textColor, for: .normal)
                if (type == .rounded) {
                    btn.layer.borderWidth = bdrWidth
                    btn.layer.borderColor = bdrColor.cgColor
                }
            }
        }
        
        for indi in indis  {
            if (indi.tag == selected) {
                indi.backgroundColor = selIndiColor
            }
            else {
                indi.backgroundColor = indiColor
            }
        }
    }
    
    func getSelected() -> Int {
        return selected
    }

    override init(frame: CGRect) {
        super.init(frame: frame)
        sharedInit()
    }
    
    required public init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        sharedInit()
    }

    private var selected: Int = -1
    private var btns: [UIButton] = []
    private var indis: [UIView] = []
    private var sendEvents: Bool = false
    
    func sharedInit() {
        showsVerticalScrollIndicator = false
        showsHorizontalScrollIndicator = false
        clipsToBounds = true
    }
    
    func reloadData() {
        contentSize = CGSize(width: 0.0, height: 0.0)
        contentOffset = CGPoint(x: 0.0, y: 0.0)
        btns = []
        indis = []
        selected = -1
        for view in subviews {
            view.removeFromSuperview()
        }
        if (items.count > 0) {
            for index in 0..<items.count {
                let btn = UIButton(type: .custom)
                btns.append(btn)
                btn.tag = index
                btn.backgroundColor = btnColor
                btn.titleLabel?.font = font
                btn.setTitleColor(textColor, for: .normal)
                btn.setTitle(items[index], for: .normal)
                if (type == .rounded) {
                    btn.layer.borderWidth = bdrWidth
                    btn.layer.borderColor = bdrColor.cgColor
                }
                addSubview(btn)
                btn.addTarget(self, action: #selector(btnClicked), for: .touchUpInside)
                if (type == .android || type == .fixed) {
                    let indi = UIView()
                    indis.append(indi)
                    indi.tag = index
                    indi.backgroundColor = indiColor
                    addSubview(indi)
                }
            }
            layoutSubviews()
        }
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        if (items.count > 0) {
            var yPadding: CGFloat = 0.0
            if (type == .rounded) {
                yPadding = 6.0
            }
            let btnHeight: CGFloat = frame.height - (yPadding + yPadding)
            var div: CGFloat = 0.0
            if (type == .rounded) {
                div = 4.0
            }
            var x: CGFloat = xPadding + div
            var width: CGFloat = x
            for index in 0..<items.count {
                let btn = btns[ index ]
                if (type == .rounded) {
                    btn.layer.cornerRadius = btnHeight / 2.0
                }
                else {
                    btn.layer.cornerRadius = 0.0
                }
                var size = btn.sizeThatFits(CGSize(width: 0.0, height: 0.0))
                size = CGSize(width: size.width + (frame.size.height / 2.0), height: btnHeight)
                if (type == .fixed) {
                    size = CGSize(width: frame.size.width / CGFloat(items.count), height: size.height)
                }
                btn.frame = CGRect(x: x, y: yPadding, width: size.width, height: size.height)
                
                if (type == .android || type == .fixed) {
                    let indi = indis[ index ]
                    indi.frame = CGRect(x: x, y: frame.height - indiHeight, width: size.width, height: indiHeight)
                }
                
                x += btn.frame.size.width
                width += btn.frame.size.width
                if (index < (items.count-1)) {
                    x += div
                    width += div
                }
                
            }
            width += (xPadding + div)
            contentSize = CGSize(width: width, height: frame.size.height)
        }
    }
    
    @objc func btnClicked(btn: UIButton) {
        select(index: btn.tag)
    }
    
    deinit {
        print("UITabView::deinit()")
    }
}

