import UIKit

struct BusyView {
    static func show (_ inView : UIView, _ backgroundColor : UIColor = UIColor.white, _ lightIndicator: Bool = false) -> Void {
        if (showing(inView) == false) {
            let indicatorView = UIActivityIndicatorView(style: .gray)
            if lightIndicator == false {
                indicatorView.style = .gray
            }
            else {
                indicatorView.style = .white
            }
            indicatorView.backgroundColor = backgroundColor
            inView.addSubview(indicatorView)
            indicatorView.frame = CGRect(x: 0.0, y: 0.0, width: inView.frame.size.width, height: inView.frame.size.height)
            indicatorView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
            indicatorView.startAnimating()
        }
    }
    
    static func hide (_ inView : UIView) -> Void {
        for subview in inView.subviews {
            if (subview is UIActivityIndicatorView) {
                subview.removeFromSuperview()
            }
        }
    }
    
    static func showing (_ inView : UIView) -> Bool {
        for subview in inView.subviews {
            if (subview is UIActivityIndicatorView) {
                return true
            }
        }
        return false
    }
    
    static func tintColor () -> UIColor {
        return UIColor.red
    }
}
