import UIKit
import GradientKit
import FirebaseAuth
import SwiftyJSON

class LoginVerifyController : UIViewController, UIScrollViewDelegate {
    @IBOutlet weak var txtField: UITextField!
    @IBOutlet weak var pin1: UILabel!
    @IBOutlet weak var pin2: UILabel!
    @IBOutlet weak var pin3: UILabel!
    @IBOutlet weak var pin4: UILabel!
    @IBOutlet weak var pin5: UILabel!
    @IBOutlet weak var pin6: UILabel!
    var otp: String = ""
        
    @IBOutlet weak var ivCheck: UIImageView!
    var checked: Bool = false
    
    var msisdn: String = ""
    var verificationID: String = ""
    
    convenience init (msisdn: String, verificationID: String) {
        self.init()
        self.msisdn = msisdn
        self.verificationID = verificationID
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .default
    }

    override func viewDidLoad() {
        super.viewDidLoad()                
        let layer1 = LinearGradientLayer(direction: .vertical)
        layer1.colors = [UIColor(hex: 0xFF61A3), UIColor(hex: 0x9648F2), UIColor(hex: 0x5A52F2)]
        layer1.frame = CGRect(x: 0, y: 0, width: UIScreen.main.bounds.width, height: UIScreen.main.bounds.height)
        view.layer.insertSublayer(layer1, at: 0)
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.isNavigationBarHidden = true
    }

    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.navigationController?.isNavigationBarHidden = false
    }

    @IBAction func btnOtpClicked(sender: UIButton?) {
        txtField.becomeFirstResponder()
    }
    
    func showDots() {
        let labels = [pin1, pin2, pin3, pin4, pin5, pin6]
        for i in 0 ..< 6 {
            if i < otp.count {
                labels[i]?.text = String(otp[i])
            }
            else {
                labels[i]?.text = " "
            }
        }
    }


    @IBAction func btnVerifyClicked(sender: UIButton?) {
        if otp.count < 6 {
            txtField.becomeFirstResponder()
            return
        }
        
        BusyView.show(self.view)
        let credential = PhoneAuthProvider.provider().credential(withVerificationID: verificationID,
                                                                   verificationCode: otp)
        Auth.auth().signInAndRetrieveData(with: credential) { authData, error in
            BusyView.hide(self.view)
            if let error = error {
                KSToastView.ks_showToast(error.localizedDescription)
            }
            else {
                self.requestRegister()
            }
        }
    }
        
    func requestRegister() {
        let params = ["msisdn" : msisdn, "country_code" : "62", "os": "iOS"] as [String : Any]
        POST(url: "http://117.54.201.46:12080/milove/api/v1/register", params: params, headers: nil, cached: false, completion: {(success: Bool, json: JSON) in
            if (success == true) {
                if json["code"].intValue == 200 {
                    KSToastView.ks_showToast("Register success.")
                    let user = json["data"][0]
                    UserDefaults.standard.set(user.rawString(), forKey: "user")
                    UserDefaults.standard.synchronize()
                    self.navigationController?.pushViewController(HomeController(), animated: false)
                }
                else {
                    KSToastView.ks_showToast("Register failed.")
                }
            }
        })

    }

    
}

extension LoginVerifyController : UITextFieldDelegate {
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        return true
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if let text = textField.text,
            let textRange = Range(range, in: text) {
            if text.count == 6 && string.count > 0 {
                return false
            }
            otp = text.replacingCharacters(in: textRange, with: string)
            showDots()
        }
        return true
    }
}
