import UIKit

class DialogInfo : UIViewController {
    @IBOutlet weak var labelTitle: UILabel!
    @IBOutlet weak var textView: UITextView!
    @IBOutlet weak var btn: UIButton!
    var dlgTitle : String = ""
    var msg : String = ""
    var btnColor : UIColor?
    var btnTitle : String?
    var centered : Bool = true
    var btnClicked: (() -> Void)? = nil
    static var controller: DialogInfo? = nil

    static func showWith(title: String, msg: String, btnTitle: String? = nil, centered: Bool = true, btnClicked: (() -> Void)? = nil) {
        if controller == nil {
            controller = DialogInfo()
        }
        controller?.dlgTitle = title
        controller?.msg = msg
        controller?.btnTitle  = btnTitle
        controller?.btnClicked = btnClicked
        controller?.centered = centered
        controller?.setPopinTransitionStyle(.slide)
        let rootController = UIApplication.shared.delegate!.window!!.rootViewController!
        rootController.view.endEditing(true)
        KeyboardAvoiding.avoidingView = controller?.view
        if controller?.btnClicked == nil {
            controller?.setPopinOptions(BKTPopinOption.ignoreKeyboardNotification)
        }
        else {
            controller?.setPopinOptions([BKTPopinOption.ignoreKeyboardNotification, BKTPopinOption.disableAutoDismiss])
        }
        rootController.presentPopinController(controller, animated: true, completion: nil)
    }

    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.labelTitle.text = dlgTitle
        if (self.centered == true) {
            self.textView.textAlignment = .center
        }
        if (self.btnColor != nil) {
            self.btn.backgroundColor = self.btnColor
        }
        if (btnTitle != nil) {
            self.btn.setTitle(btnTitle, for: UIControl.State.normal)
        }
        let size = self.view.systemLayoutSizeFitting(CGSize(width: 300.0, height: 0.0), withHorizontalFittingPriority: UILayoutPriority.required, verticalFittingPriority: UILayoutPriority.defaultLow)
        self.textView.text = msg
        var cy = self.textView.contentSize.height + size.height
        if (cy > 400.0) {
            cy = 400.0
        }
        self.view.frame = CGRect.init(x: 0.0, y: 0.0, width: 300.0, height: cy)
    }
    
    @IBAction func btnClicked(sender: UIButton?) {
        self.presentingPopin().dismissCurrentPopinController(animated: true, completion: {
            self.btnClicked?()
        })
    }
    
    deinit {
        print("DialogInfo::deinit()")
    }

}
