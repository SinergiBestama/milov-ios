import UIKit

class DialogInput : UIViewController {
    @IBOutlet weak var labelTitle: UILabel!
    @IBOutlet weak var labelMsg: UILabel!
    @IBOutlet weak var txtInput: UITextField!
    @IBOutlet weak var btnCancel: UIButton!
    @IBOutlet weak var btnOK: UIButton!
    var dlgTitle : String = ""
    var msg : String = ""
    var placeholder : String = ""
    var centered : Bool = true
    var btnCancelTitle : String? = nil
    var btnCancelClicked: (() -> Void)? = nil
    var btnOKTitle : String? = nil
    var btnOKClicked: ((_ text: String) -> Void)?
    static var controller: DialogInput? = nil

    static func showWith(title: String, msg: String, placeholder: String, centered: Bool = true, btnCancelTitle: String? = nil, btnCancelClicked: (() -> Void)? = nil, btnOKTitle: String? = nil, btnOKClicked: ((_ text: String) -> Void)? = nil) {
        if controller == nil {
            controller = DialogInput()
        }
        controller?.dlgTitle = title
        controller?.msg = msg
        controller?.placeholder = placeholder
        controller?.centered = centered
        controller?.btnCancelTitle = btnCancelTitle
        controller?.btnCancelClicked = btnCancelClicked
        controller?.btnOKTitle = btnOKTitle
        controller?.btnOKClicked = btnOKClicked
        controller?.setPopinTransitionStyle(.slide)
        let rootController = UIApplication.shared.delegate!.window!!.rootViewController!
        rootController.view.endEditing(true)
        KeyboardAvoiding.avoidingView = controller?.view
        rootController.setPopinOptions(BKTPopinOption.ignoreKeyboardNotification)
        rootController.presentPopinController(controller, animated: true, completion: nil)
    }

    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.labelTitle.text = dlgTitle
        self.labelMsg.text = msg
        self.txtInput.placeholder = placeholder
        if (self.centered == true) {
            self.labelMsg.textAlignment = .center
        }
        if (btnCancelTitle != nil) {
            self.btnCancel.setTitle(btnCancelTitle, for: UIControl.State.normal)
        }
        if (btnOKTitle != nil) {
            self.btnOK.setTitle(btnOKTitle, for: UIControl.State.normal)
        }
    }
    
    @IBAction func btnCancelClicked(sender: UIButton?) {
        self.presentingPopin().dismissCurrentPopinController(animated: true, completion: {
            self.btnCancelClicked?()
        })
    }
    
    @IBAction func btnOKClicked(sender: UIButton?) {
        if txtInput.text != nil && txtInput.text?.count == 0 {
            txtInput.becomeFirstResponder()
        }
        else {
            self.presentingPopin().dismissCurrentPopinController(animated: true, completion: {
                self.btnOKClicked?(self.txtInput.text!)
            })
        }
    }
}

extension DialogInput : UITextFieldDelegate {
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.btnOKClicked(sender: nil)
        return true
    }
}

