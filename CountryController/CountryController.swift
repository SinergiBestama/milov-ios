import UIKit
import SwiftyJSON
import SwiftDate
import GradientKit

class CountryController : UIViewController {
    @IBOutlet weak var tableView: UITableView!
    let pageSize: Int = 8
    var requesting: Bool = false
    var page : Int = 1
    var newsList : [JSON] = []
    
    let countryNameList = ["Indonesia", "Malaysia", "Thailand", "Pakistan", "Vietnam", "Singapore"]
    let flagList = ["ID.png", "MY.png", "TH.png", "PK.png", "VN.png", "SG.png"]
    let codeList = ["+62", "+60", "+66", "+92", "+84", "+65"]
    

    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        self.tableView.register(UINib(nibName: CountryCell.className(), bundle: nil), forCellReuseIdentifier: CountryCell.className())
        
        let layer1 = LinearGradientLayer(direction: .vertical)
        layer1.colors = [UIColor(hex: 0xFF61A3), UIColor(hex: 0x9648F2), UIColor(hex: 0x5A52F2)]
        layer1.frame = CGRect(x: 0, y: 0, width: UIScreen.main.bounds.width, height: UIScreen.main.bounds.height)
        view.layer.insertSublayer(layer1, at: 0)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.isNavigationBarHidden = true
    }

    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.navigationController?.isNavigationBarHidden = false
    }

    
    @IBAction func btnNextClicked(sender: UIButton?) {
        navigationController?.popViewController(animated: false)
    }
    
    func requestNewsList() {
        if (self.requesting == true) {
            return
        }
        
        self.requesting = true
        let params = ["page" : self.page, "per_page" : pageSize] as [String : Any]
        POST(url: "http://45.32.103.6/api/public/news/list", params: params, headers: nil, cached: false, completion: {(success: Bool, json: JSON) in
            self.requesting = false
            self.tableView.pullToRefreshView.stopAnimating()
            if (success == true) {
                let data = json["result"]["data"]
                if (data.arrayValue.count > 0) {
                    if ((params["page"] as! Int) <= 1) {
                        UserDefaults.standard.set(data.rawString(), forKey: "newsList")
                        UserDefaults.standard.synchronize()
                        self.newsList = []
                    }
                    for item in data.arrayValue {
                        self.newsList.append(item)
                    }
                    self.reloadNewsList()
                    self.page = self.page + 1
                }
            }
        })
    }
    
    func reloadNewsList() {
        if (self.page <= 1) {
            if let json = UserDefaults.standard.string(forKey: "newsList") {
                newsList = JSON.parse(json).arrayValue
            }
        }
        self.tableView.reloadData()
    }
    
    deinit {
    }
}

extension CountryController : DZNEmptyDataSetSource, DZNEmptyDataSetDelegate {
    func image(forEmptyDataSet scrollView: UIScrollView!) -> UIImage! {
        return UIImage(named: "ic_empty_dataset.png")
    }
    
    func description(forEmptyDataSet scrollView: UIScrollView!) -> NSAttributedString! {
        let titleAttr = [NSAttributedString.Key.foregroundColor: UIColor.darkGray, NSAttributedString.Key.font: UIFont(name: "HelveticaNeue", size: 12.0)!]
        let titleText = NSMutableAttributedString(string: "Data belum tersedia.\n", attributes: titleAttr)
        let descAttr = [NSAttributedString.Key.foregroundColor: UIColor.lightGray, NSAttributedString.Key.font: UIFont(name: "HelveticaNeue", size: 12.0)!]
        let descText = NSMutableAttributedString(string: "Coba lakukan refresh untuk menarik data terbaru. Periksa kembali koneksi internet anda jika masih tidak berhasil.", attributes: descAttr)
        let text = NSMutableAttributedString()
        text.append(titleText)
        text.append(descText)
        return text
    }
    
    func emptyDataSetShouldAllowScroll(_ scrollView: UIScrollView!) -> Bool {
        return true
    }
    
    func verticalOffset(forEmptyDataSet scrollView: UIScrollView!) -> CGFloat {
        return 0
    }
    
    func buttonTitle(forEmptyDataSet scrollView: UIScrollView!, for state: UIControl.State) -> NSAttributedString! {
        let title = NSLocalizedString("Refresh", comment: "")
        let attr = [NSAttributedString.Key.foregroundColor: UIColor.darkGray, NSAttributedString.Key.font: UIFont(name: "HelveticaNeue-Medium", size: 12.0)!]
        let attrString = NSAttributedString(string: title, attributes: attr)
        return attrString
    }
    
    func emptyDataSet(_ scrollView: UIScrollView!, didTap button: UIButton!) {
        self.tableView.triggerPullToRefresh()
    }
}

extension CountryController : UITableViewDataSource, UITableViewDelegate {
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.countryNameList.count
    }
        
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: CountryCell.className(), for: indexPath) as! CountryCell
        cell.iv.source(url: flagList[indexPath.row])
        cell.labelTitle.text = countryNameList[indexPath.row]
        cell.labelCode.text = codeList[indexPath.row]
        return cell
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        if (indexPath.row == (self.newsList.count-pageSize) || indexPath.row == (self.newsList.count-1)) {
            DispatchQueue.main.async {
                self.requestNewsList()
            }
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath as IndexPath, animated: true)
        navigationController?.popViewController(animated: false)
    }
    
}
