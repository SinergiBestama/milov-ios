import UIKit
import Alamofire
import AlamofireImage

class CountryCell: UITableViewCell {
    @IBOutlet weak var iv: UIImageView!
    @IBOutlet weak var labelTitle: UILabel!
    @IBOutlet weak var labelCode: UILabel!
}
