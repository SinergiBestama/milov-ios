import UIKit

class DialogQuestion : UIViewController {
    @IBOutlet weak var labelTitle: UILabel!
    @IBOutlet weak var textView: UITextView!
    @IBOutlet weak var btnCancel: UIButton!
    @IBOutlet weak var btnOK: UIButton!
    var dlgTitle : String = ""
    var msg : String = ""
    var centered : Bool = true
    var btnCancelTitle : String? = nil
    var btnCancelClicked: (() -> Void)? = nil
    var btnOKTitle : String? = nil
    var btnOKClicked: (() -> Void)? = nil
    static var controller: DialogQuestion? = nil

    static func showWith(title: String, msg: String, centered: Bool = true, btnCancelTitle: String? = nil, btnCancelClicked: (() -> Void)? = nil, btnOKTitle: String? = nil, btnOKClicked: (() -> Void)? = nil) {
        if controller == nil {
            controller = DialogQuestion()
        }
        controller?.dlgTitle = title
        controller?.msg = msg
        controller?.centered = centered
        controller?.btnCancelTitle = btnCancelTitle
        controller?.btnCancelClicked = btnCancelClicked
        controller?.btnOKTitle = btnOKTitle
        controller?.btnOKClicked = btnOKClicked
        controller?.setPopinTransitionStyle(.slide)
        let rootController = UIApplication.shared.delegate!.window!!.rootViewController!
        rootController.view.endEditing(true)
        KeyboardAvoiding.avoidingView = controller?.view
        rootController.setPopinOptions(BKTPopinOption.ignoreKeyboardNotification)
        rootController.presentPopinController(controller, animated: true, completion: nil)
    }

    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.labelTitle.text = dlgTitle
        if (self.centered == true) {
            self.textView.textAlignment = .center
        }
        if (btnCancelTitle != nil) {
            self.btnCancel.setTitle(btnCancelTitle, for: UIControl.State.normal)
        }
        if (btnOKTitle != nil) {
            self.btnOK.setTitle(btnOKTitle, for: UIControl.State.normal)
        }
        let size = self.view.systemLayoutSizeFitting(CGSize(width: 300.0, height: 0.0), withHorizontalFittingPriority: UILayoutPriority.required, verticalFittingPriority: UILayoutPriority.defaultLow)
        self.textView.text = msg
        var cy = self.textView.contentSize.height + size.height
        if (cy > 400.0) {
            cy = 400.0
        }
        self.view.frame = CGRect.init(x: 0.0, y: 0.0, width: 300.0, height: cy)
    }
    
    @IBAction func btnCancelClicked(sender: UIButton?) {
        self.presentingPopin().dismissCurrentPopinController(animated: true, completion: {
            self.btnCancelClicked?()
        })
    }
    
    @IBAction func btnOKClicked(sender: UIButton?) {
        self.presentingPopin().dismissCurrentPopinController(animated: true, completion: {
            self.btnOKClicked?()
        })
    }
}
