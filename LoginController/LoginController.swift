import UIKit
import GradientKit
import FirebaseAuth
import KSToastView

class LoginController : UIViewController, UIScrollViewDelegate {
    @IBOutlet weak var ivCheck: UIImageView!
    @IBOutlet weak var txtPhoneNumber: UITextField!
    var checked: Bool = false

    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .default
    }

    override func viewDidLoad() {
        super.viewDidLoad()        
        let layer1 = LinearGradientLayer(direction: .vertical)
        layer1.colors = [UIColor(hex: 0xFF61A3), UIColor(hex: 0x9648F2), UIColor(hex: 0x5A52F2)]
        layer1.frame = CGRect(x: 0, y: 0, width: UIScreen.main.bounds.width, height: UIScreen.main.bounds.height)
        view.layer.insertSublayer(layer1, at: 0)
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.isNavigationBarHidden = true
    }

    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.navigationController?.isNavigationBarHidden = false
    }

    @IBAction func btnCountryClicked(sender: UIButton?) {
        self.navigationController?.pushViewController(CountryController(), animated: false)
    }

    @IBAction func btnCheckClicked(sender: UIButton?) {
        if checked == false {
            checked = true
            ivCheck.image = UIImage(named: "ic_checked.png")
        }
        else {
            checked = false
            ivCheck.image = nil
        }
    }

    @IBAction func btnNextClicked(sender: UIButton?) {
        if txtPhoneNumber.text == nil || txtPhoneNumber.text?.count == 0 {
            txtPhoneNumber.becomeFirstResponder()
            return
        }
        
        BusyView.show(self.view)
        let msisdn = txtPhoneNumber.text!
        PhoneAuthProvider.provider().verifyPhoneNumber(txtPhoneNumber.text!, uiDelegate: nil) { (verificationID, error) in
            BusyView.hide(self.view)
            if let error = error {
                KSToastView.ks_showToast(error.localizedDescription)
            }
            else {
                KSToastView.ks_showToast("OTP sent.")
                self.navigationController?.pushViewController(LoginVerifyController(msisdn: msisdn, verificationID:  verificationID!), animated: true)
            }
        }
    }
    
}
