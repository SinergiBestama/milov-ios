import UIKit
import GradientKit

class OnboardingController : UIViewController, UIScrollViewDelegate {
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var pageControl: UIPageControl!
    @IBOutlet weak var btnNext: UIButton!
    
    var pages: [UIView] = []    

    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .default
    }

    override func viewDidLoad() {
        super.viewDidLoad()
                        
        let page1: UIView = Bundle.main.loadNibNamed("OnboardingPage1", owner: nil, options: nil)?.first as! UIView
        let page2: UIView = Bundle.main.loadNibNamed("OnboardingPage2", owner: nil, options: nil)?.first as! UIView
        let page3: UIView = Bundle.main.loadNibNamed("OnboardingPage3", owner: nil, options: nil)?.first as! UIView
        pages = [page1, page2, page3]
        for subview in pages {
            scrollView.addSubview(subview)
        }
                
        pageControl.currentPage = 0
        pageControl.numberOfPages = scrollView.subviews.count
        viewDidLayoutSubviews()
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        scrollView.contentSize = CGSize(width: scrollView.frame.size.width * CGFloat(pages.count), height: scrollView.frame.size.height)
        var index: Int = 0
        for subview in scrollView.subviews {
            subview.frame = CGRect.init (x: CGFloat(index) * self.view.frame.size.width, y: 0.0, width: scrollView.frame.size.width, height: scrollView.frame.size.height);
            scrollView.bringSubviewToFront(subview)
            index = index + 1
        }
    }

    @IBAction func btnBackClicked(sender: UIButton?) {
        navigationController?.popViewController(animated: true)
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.isNavigationBarHidden = true
    }

    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.navigationController?.isNavigationBarHidden = false
    }

    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        let pageIndex = lround (Double(self.scrollView.contentOffset.x / self.scrollView.frame.size.width));
        if pageIndex != pageControl.currentPage {
            pageControl.currentPage = pageIndex;
        }
    }

    @IBAction func btnSkipClicked(sender: UIButton?) {
        UserDefaults.standard.set(true, forKey: "onboarding")
        UserDefaults.standard.synchronize()

        var controller: LoginController? = NSCACHE["LoginController"] as? LoginController
        if controller == nil {
            controller = LoginController()
            NSCACHE["LoginController"] = controller
        }
        self.navigationController?.pushViewController(controller!, animated: true)
    }
    
    deinit {
    }
}
