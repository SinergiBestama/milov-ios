import UIKit

extension UIButton {
    private func image(withColor color: UIColor) -> UIImage? {
        let rect = CGRect(x: 0.0, y: 0.0, width: 1.0, height: 1.0)
        UIGraphicsBeginImageContext(rect.size)
        let context = UIGraphicsGetCurrentContext()
        context?.setFillColor(color.cgColor)
        context?.fill(rect)
        let image = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        return image
    }
    
    func setBackgroundColor(_ color: UIColor, for state: UIControl.State) {
        self.setBackgroundImage(image(withColor: color), for: state)
    }
    
    override open var backgroundColor: UIColor? {
        didSet {
            if let color = backgroundColor {
                setBackgroundColor(color.darker(0.25), for: .highlighted)
            }
        }
    }

    typealias UIButtonClickedAction = (UIButton) -> ()
    static var clickedActionKey = "clickedActionKey"
    func setClicked(_ closure: @escaping UIButtonClickedAction) {
        objc_setAssociatedObject(self, &UIButton.clickedActionKey, closure, objc_AssociationPolicy.OBJC_ASSOCIATION_RETAIN_NONATOMIC)
        removeTarget(self, action: #selector(clickedAction), for: .touchUpInside)
        addTarget(self, action: #selector(clickedAction), for: .touchUpInside)
    }
    
    @objc func clickedAction() {
        let action: UIButtonClickedAction? = objc_getAssociatedObject(self, &UIButton.clickedActionKey) as! UIButtonClickedAction?
        action!(self)
    }
}
