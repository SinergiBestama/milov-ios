import UIKit

extension UIImage{
    
    static func GenerateQR(code: String, width: CGFloat, height: CGFloat) -> UIImage? {
        let data = code.data(using: String.Encoding.ascii)
        if let filter = CIFilter(name: "CIQRCodeGenerator") {
            filter.setValue(data, forKey: "inputMessage")
            filter.setValue("H", forKey: "inputCorrectionLevel")
            if let qrCodeImage = filter.outputImage {
                let transform = CGAffineTransform(scaleX: width / qrCodeImage.extent.size.width, y: height / qrCodeImage.extent.size.height)
                return UIImage(ciImage: qrCodeImage.transformed(by: transform))
            }
        }
        return nil
    }
    
    func resizeImageWith(newSize: CGSize) -> UIImage {
        let horizontalRatio = newSize.width / size.width
        let verticalRatio = newSize.height / size.height
        let ratio = max(horizontalRatio, verticalRatio)
        let newSize = CGSize(width: size.width * ratio, height: size.height * ratio)
        UIGraphicsBeginImageContextWithOptions(newSize, true, 1.0)
        draw(in: CGRect(origin: CGPoint(x: 0, y: 0), size: newSize))
        let newImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        return newImage!
    }
    
}
