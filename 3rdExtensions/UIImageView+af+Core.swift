import UIKit
import Alamofire
import AlamofireImage

extension UIImageView {
    func source(url: String, placeholder: String = "") {
        if url.count == 0 {
            if placeholder.count > 0 {
                self.image = UIImage(named: placeholder)
            }
        }
        else {
            if url.range(of: "http://") == nil && url.range(of: "https://") == nil {
                self.image = UIImage(named: url)
            }
            else {
                self.af_setImage(withURL: URL(string: url.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)!)!, placeholderImage: UIImage(named: placeholder))
            }
        }
    }
}
