import UIKit

extension String {
    func dateValue() -> Date {
        if count == 0 {
            return Date(timeIntervalSince1970: 0)
        }
        if range(of: "T") != nil {
            if range(of: "+") != nil {
                let formatter = DateFormatter()
                formatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ssZZZZZ"
                return formatter.date(from: self) ?? Date(timeIntervalSince1970: 0)
            }
            else {
                let formatter = DateFormatter()
                formatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSSZ"
                return formatter.date(from: self) ?? Date(timeIntervalSince1970: 0)
            }
        }
        if range(of: "-") != nil {
            let formatter = DateFormatter()
            formatter.dateFormat = "yyyy-MM-dd"
            return formatter.date(from: self) ?? Date(timeIntervalSince1970: 0)
        }
        if range(of: "/") != nil {
            let formatter = DateFormatter()
            formatter.dateFormat = "yyyy/MM/dd"
            return formatter.date(from: self) ?? Date(timeIntervalSince1970: 0)
        }
        return Date(timeIntervalSince1970: TimeInterval(self.intValue()) / 1000)
    }

}
