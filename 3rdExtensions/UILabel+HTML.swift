import UIKit

extension UILabel {
    func setHTMLText(_ html: String) {
        let data = html.data(using: String.Encoding.unicode)!
        let str = try! NSMutableAttributedString (data: data, options: [NSAttributedString.DocumentReadingOptionKey.documentType: NSAttributedString.DocumentType.html], documentAttributes: nil)
        let attr = [NSAttributedString.Key.foregroundColor: self.textColor, NSAttributedString.Key.font: self.font] as [NSAttributedString.Key : Any]
        str.addAttributes(attr, range: NSRange.init(location: 0, length: str.length))
        attributedText = str
    }
}
