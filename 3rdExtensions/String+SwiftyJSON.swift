import UIKit
import SwiftyJSON

extension String {
    func json() -> JSON {
        return JSON.parse(self)
    }
}

extension JSON {
    func string() -> String {
        if let str = self.rawString() {
            return str
        }
        else {
            return ""
        }
    }
}
