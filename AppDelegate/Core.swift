import UIKit
import CoreLocation
import SwiftyJSON

func validEmail(address: String?) -> Bool {
    let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,64}"
    let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
    return emailTest.evaluate(with: address)
}

func callTo(_ phone: String?) {
    if (phone != nil && phone!.count > 0) {
        var number = phone
        number = number?.replacingOccurrences(of: " ", with: "")
        number = number?.replacingOccurrences(of: "(", with: "")
        number = number?.replacingOccurrences(of: ")", with: "")
        number = number?.replacingOccurrences(of: "-", with: "")
        let url = URL(string: "tel://" + number!)
        UIApplication.shared.openURL(url!)
    }
}

func mailTo(_ addr: String?) {
    if (addr != nil && addr!.count > 0) {
        let url = URL(string: "mailto://" + addr!)
        UIApplication.shared.openURL(url!)
    }
}

func navigateTo(_ addr: String?) {
    if (addr != nil && addr!.count > 0) {
        var value : String = addr!
        if (value.hasPrefix("http") == false) {
            value = "http://" + value
        }
        let url = URL(string: value)
        UIApplication.shared.openURL(url!)
    }
}

func locationServicesEnabled() -> Bool {
    if (CLLocationManager.locationServicesEnabled() == true) {
        switch CLLocationManager.authorizationStatus() {
        case .notDetermined, .restricted, .denied:
            return false
        case .authorizedAlways, .authorizedWhenInUse:
            return true
        }
    } else {
        return false
    }
}

// Dont forget to add Info/URL schemes: "prefs" and "App-prefs".
func openSettings(url: String) {
    guard let settingsUrl = URL(string: url), UIApplication.shared.canOpenURL(settingsUrl) else { return }
    if #available(iOS 10.0, *) {
        UIApplication.shared.open(settingsUrl)
    } else {
        UIApplication.shared.openURL(settingsUrl)
    }
}

func openLocationServicesSettings() {
    if #available(iOS 10.0, *) {
        openSettings(url: "App-Prefs:root=Privacy&path=LOCATION")
    } else {
        openSettings(url: "prefs:root=LOCATION_SERVICES")
    }
}


func isIpad() -> Bool {
    switch UIDevice.current.userInterfaceIdiom {
    case .phone:
        break
    case .pad:
        return true
    case .tv:
        break
    case .carPlay:
        break
    case .unspecified:
        break
    }
    return false
}

