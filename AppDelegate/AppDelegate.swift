import UIKit
import CoreLocation
import UserNotifications
import FirebaseCore
import FirebaseInstanceID
import FirebaseMessaging
import FirebaseAuth
import GoogleMaps
import GoogleSignIn
import FBSDKCoreKit
import FBSDKLoginKit
import SwiftyJSON
import Crashlytics

var locationLongitude: CLLocationDegrees = 0.0
var locationLatitude: CLLocationDegrees = 0.0

var app: AppDelegate? = nil
@UIApplicationMain class AppDelegate: UIResponder, UIApplicationDelegate, CLLocationManagerDelegate, MessagingDelegate  {
    var window: UIWindow?
    var navigationController: UINavigationController?
    var locationManager: CLLocationManager?

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        app = self        
        NSSetUncaughtExceptionHandler { exception in
            print("*** NSSetUncaughtExceptionHandler ***")
            print(exception)
            print(exception.callStackSymbols)
        }
        
#if DEBUG
        NetworkActivityLogger.shared.level = .debug
        NetworkActivityLogger.shared.startLogging()
#endif

        UINavigationBar.appearance().isTranslucent = false
        UINavigationBar.appearance().barStyle = .blackOpaque
        UINavigationBar.appearance().barTintColor = .white
        UINavigationBar.appearance().tintColor = .darkGray
        let titleAttrs = [
            NSAttributedString.Key.foregroundColor: UIColor.darkGray,
            NSAttributedString.Key.font: UIFont(name: "HelveticaNeue-Medium", size: 14.0)!
        ]
        UINavigationBar.appearance().titleTextAttributes = titleAttrs
        UIButton.appearance().clipsToBounds = true
        let barButtonItemAttr = [
            NSAttributedString.Key.foregroundColor: UIColor.white,
            NSAttributedString.Key.font: UIFont(name: "HelveticaNeue-Medium", size: 14.0)!
        ]
        UIBarButtonItem.appearance().setTitleTextAttributes(barButtonItemAttr, for: UIControl.State.normal)
        UIBarButtonItem.appearance().setTitleTextAttributes(barButtonItemAttr, for: UIControl.State.highlighted)

        UITabBar.appearance().tintColor = UIColor(hex: 0xE07D48)
        UITabBar.appearance().barTintColor = UIColor(hex: 0xF3F3F3)
        let tabBarItemAttr = [
            NSAttributedString.Key.foregroundColor: UIColor.darkGray,
            NSAttributedString.Key.font: UIFont(name: "HelveticaNeue-Medium", size: 12.0)!
        ]
        let selectedTabBarItemAttr = [
            NSAttributedString.Key.foregroundColor: UIColor(hex: 0xE07D48),
            NSAttributedString.Key.font: UIFont(name: "HelveticaNeue-Medium", size: 12.0)!
        ]
        UITabBarItem.appearance().setTitleTextAttributes(tabBarItemAttr, for: .normal)
        UITabBarItem.appearance().setTitleTextAttributes(selectedTabBarItemAttr, for: .selected)
        
        self.window = UIWindow(frame: UIScreen.main.bounds)
        self.navigationController = UINavigationController()
        self.navigationController?.view.backgroundColor = UIColor.white
        self.window?.rootViewController = self.navigationController
        self.window?.makeKeyAndVisible()
        
        if UserDefaults.standard.bool(forKey: "onboarding") == false {
            self.navigationController?.pushViewController(OnboardingController(), animated: false)
        }
        else {
            if UserDefaults.standard.string(forKey: "user") != nil {
                self.navigationController?.pushViewController(HomeController(), animated: false)
            }
            else {
                self.navigationController?.pushViewController(LoginController(), animated: false)
            }
        }

        self.navigationController?.viewControllers = [HomeController()]

        // Firebase config: Don't forget to add FirebaseAppDelegateProxyEnabled = NO in info.plist
        // Also add: GoogleService-Info.plist
        // Other linker flag: -objC
        FirebaseApp.configure()
        
        Messaging.messaging().delegate = self
        let uns: UIUserNotificationSettings = UIUserNotificationSettings(types: [.alert, .badge, .sound], categories: nil)
        application.registerUserNotificationSettings(uns)
        application.registerForRemoteNotifications()
        
        // Google SignIn
        GIDSignIn.sharedInstance().clientID = FirebaseApp.app()?.options.clientID
        
        // Facebook Login
        FBSDKSettings.setAppID("177728120003591")
        FBSDKSettings.setDisplayName(Bundle.main.object(forInfoDictionaryKey: "CFBundleDisplayName") as? String)
        
        // Location config
        locationManager = CLLocationManager()
        locationManager?.delegate = self;
        locationManager?.desiredAccuracy = kCLLocationAccuracyKilometer
        locationManager?.requestWhenInUseAuthorization()
        locationManager?.startUpdatingLocation()
        
        // Maps config
        GMSServices.provideAPIKey("AIzaSyDtD-hhjpFjoH9J133A_YtJDMzLA8JPE-o")

                                
        return FBSDKApplicationDelegate.sharedInstance().application(application, didFinishLaunchingWithOptions: launchOptions)
    }
 
    func applicationDidBecomeActive(_ application: UIApplication) {
        application.applicationIconBadgeNumber = 0
    }
    
    func application(_ application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data) {
        let apnsToken = deviceToken.reduce("", {$0 + String(format: "%02x", $1)})
        print("[FCM] didRegisterForRemoteNotificationsWithDeviceToken:\nAPNS Token: \(apnsToken)")
        let defaults = UserDefaults.standard
        defaults.set(apnsToken, forKey: "apns-token")
        defaults.synchronize()
        Messaging.messaging().setAPNSToken(deviceToken, type: .unknown)
    }
    
    func application(_ application: UIApplication, didFailToRegisterForRemoteNotificationsWithError error: Error) {
        print("[FCM] didFailToRegisterForRemoteNotificationsWithError")
    }
    
    func application(_ application: UIApplication, didReceiveRemoteNotification data: [AnyHashable : Any]) {
        print("[FCM] didReceiveRemoteNotification:\n\(String(data: try! JSONSerialization.data(withJSONObject: data, options: .prettyPrinted), encoding: .utf8)!)")
    }
    
    func messaging(_ messaging: Messaging, didReceiveRegistrationToken fcmToken: String) {
        print("[FCM] didReceiveRegistrationToken: \(fcmToken)")
        self.messaging(messaging, didRefreshRegistrationToken: fcmToken)
    }
    
    func messaging(_ messaging: Messaging, didRefreshRegistrationToken fcmToken: String) {
        print("[FCM] didRefreshRegistrationToken: \(fcmToken)")
        let defaults = UserDefaults.standard
        defaults.set(fcmToken, forKey: "fcmToken")
        defaults.synchronize()
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        let location : CLLocation = locations.last!
        locationLongitude = location.coordinate.longitude
        locationLatitude = location.coordinate.latitude
        NotificationCenter.default.post(name: Notification.Name("didUpdateLocations"), object: nil)
    }
    
    func application(_ application: UIApplication, open url: URL, options: [UIApplication.OpenURLOptionsKey : Any] = [:]) -> Bool {
        if Auth.auth().canHandle(url) {
          return true
        }

        if let scheme = url.scheme {
            if scheme == "fb177728120003591" {
                return FBSDKApplicationDelegate.sharedInstance().application(application, open: url, sourceApplication: options[UIApplication.OpenURLOptionsKey.sourceApplication] as? String, annotation: [:])
            } else if scheme == "com.googleusercontent.apps.901191431136-qg4hd24l7jqg1fjhr50qopmld8vcl26e" {
                return GIDSignIn.sharedInstance().handle(url, sourceApplication:options[UIApplication.OpenURLOptionsKey.sourceApplication] as? String, annotation: [:])
            }
        }
        return false
    }
    
    func applicationWillResignActive(_ application: UIApplication) {
        FBSDKAppEvents.activateApp()
    }
    
    func application(_ application: UIApplication,
        didReceiveRemoteNotification notification: [AnyHashable : Any],
        fetchCompletionHandler completionHandler: @escaping (UIBackgroundFetchResult) -> Void) {
      if Auth.auth().canHandleNotification(notification) {
        completionHandler(.noData)
        return
      }
      // This notification is not auth related, developer should handle it.
    }
    
    // For iOS 8-
    func application(_ application: UIApplication,
                     open url: URL,
                     sourceApplication: String?,
                     annotation: Any) -> Bool {
      if Auth.auth().canHandle(url) {
        return true
      }
      // URL not auth related, developer should handle it.
        return false
    }
}
