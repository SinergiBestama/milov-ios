# GradientKit

[![CI Status](http://img.shields.io/travis/Aashish Dhawan/GradientKit.svg?style=flat)](https://travis-ci.org/Aashish Dhawan/GradientKit)
[![Version](https://img.shields.io/cocoapods/v/GradientKit.svg?style=flat)](http://cocoapods.org/pods/GradientKit)
[![License](https://img.shields.io/cocoapods/l/GradientKit.svg?style=flat)](http://cocoapods.org/pods/GradientKit)
[![Platform](https://img.shields.io/cocoapods/p/GradientKit.svg?style=flat)](http://cocoapods.org/pods/GradientKit)

## Example

To run the example project, clone the repo, and run `pod install` from the Example directory first.

## Requirements

## Installation

GradientKit is available through [CocoaPods](http://cocoapods.org). To install
it, simply add the following line to your Podfile:

```ruby
pod "GradientKit"
```

## Author

Aashish Dhawan, aashishdhawan@gmail.com

## License

GradientKit is available under the MIT license. See the LICENSE file for more info.
