import UIKit
import GoogleMaps
import SwiftyJSON

class ProfileController : UIViewController, GMSMapViewDelegate, SidebarControllerDelegate {
    var sidebarController: SidebarController?

    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .default
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        navigationItem.leftBarButtonItem = UIBarButtonItem(image: UIImage(named: "ic_burger_menu.png"), style: .plain, target: self, action: #selector(btnSidebarClicked))
        
        self.title = NSLocalizedString("My Profile", comment: "")
        
        navigationItem.rightBarButtonItem = UIBarButtonItem(image: UIImage(named: "ic_notification.png"), style: .plain, target: self, action: #selector(btnNotificationClicked))
    }
        
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.isNavigationBarHidden = false
    }

    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.navigationController?.isNavigationBarHidden = true
    }

    @IBAction func btnSidebarClicked(sender: UIButton?) {
        self.sidebarController = SidebarController()
        self.sidebarController?.delegate = self
        sidebarController?.show()
    }

    func sidebarDidHide() {
        self.sidebarController = nil
    }
    
    @IBAction func btnNotificationClicked(sender: UIButton?) {
    }

    @IBAction func btnHomeClicked(sender: UIButton?) {
    }

    @IBAction func btnExploreClicked(sender: UIButton?) {
    }

    @IBAction func btnScanClicked(sender: UIButton?) {
    }

    @IBAction func btnMatchedClicked(sender: UIButton?) {
    }

    @IBAction func btnYouClicked(sender: UIButton?) {
    }

}
