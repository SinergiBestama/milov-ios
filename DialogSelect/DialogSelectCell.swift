import UIKit

class DialogSelectCell: UITableViewCell {
    @IBOutlet weak var labelName: UILabel!
    @IBOutlet weak var ivCheck: UIImageView!
    static func rowHeight () -> CGFloat {
        return 32.0
    }
}
