import UIKit

enum SelectCellStyle {
    case standard
    case image
}

class DialogSelect : UIViewController, UITableViewDataSource, UITableViewDelegate {
    @IBOutlet weak var labelTitle: UILabel!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var btnCancel: UIButton!
    @IBOutlet weak var btnOK: UIButton!
    var dlgTitle : String = ""
    var items: [String] = []
    var images: [String]?
    var btnCancelTitle : String? = nil
    var btnOKTitle : String? = nil
    var btnOKClicked: ((_ selected: Int) -> Void)?
    var selected: Int = -1
    var style: SelectCellStyle = .standard
    static var controller: DialogSelect? = nil
    
    static func showWith(title: String, items: [String], images: [String]? = nil, selected: Int = -1, btnCancelTitle: String? = nil, btnOKTitle: String? = nil, btnOKClicked: ((_ selected: Int) -> Void)? = nil) {
        if controller == nil {
            controller = DialogSelect()
        }
        controller?.dlgTitle = title
        controller?.items = items
        controller?.images = images
        controller?.btnCancelTitle = btnCancelTitle
        controller?.btnOKTitle = btnOKTitle
        controller?.btnOKClicked = btnOKClicked
        if ((controller?.items.count)! > 0) {
            controller?.selected = selected
        }
        else {
            controller?.selected = -1
        }
        controller?.setPopinTransitionStyle(.slide)
        let rootController = UIApplication.shared.delegate!.window!!.rootViewController!
        rootController.view.endEditing(true)
        KeyboardAvoiding.avoidingView = controller?.view
        rootController.setPopinOptions(BKTPopinOption.ignoreKeyboardNotification)
        rootController.presentPopinController(controller, animated: true, completion: nil)
        
        controller?.tableView.reloadData()
        if selected > -1 {
            controller?.tableView.scrollToRow(at: IndexPath(row: selected, section: 0), at: .middle, animated: false)
        }
    }

    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        if (self.style == .standard) {
            tableView.register(UINib(nibName: "DialogSelectCell", bundle: nil), forCellReuseIdentifier: "DialogSelectCell")
        }
        else
        if (self.style == .image) {
            tableView.register(UINib(nibName: "DialogSelectImageCell", bundle: nil), forCellReuseIdentifier: "DialogSelectImageCell")
        }

        self.labelTitle.text = dlgTitle
        if (btnCancelTitle != nil) {
            self.btnCancel.setTitle(btnCancelTitle, for: UIControl.State.normal)
        }
        if (btnOKTitle != nil) {
            self.btnOK.setTitle(btnOKTitle, for: UIControl.State.normal)
        }

        let size = self.view.systemLayoutSizeFitting(CGSize(width: 300.0, height: 0.0), withHorizontalFittingPriority: UILayoutPriority.required, verticalFittingPriority: UILayoutPriority.defaultLow)
        var cy : CGFloat = 0.0
        if (self.style == .standard) {
            cy = (CGFloat(self.items.count) * DialogSelectCell.rowHeight()) + size.height
        }
        else
        if (self.style == .image) {
            cy = (CGFloat(self.items.count) * DialogSelectImageCell.rowHeight()) + size.height
        }
        if (cy > 400.0) {
            cy = 400.0
        }
        self.view.frame = CGRect.init(x: 0.0, y: 0.0, width: 300.0, height: cy)
    }
    
    @IBAction func btnCancelClicked(sender: UIButton?) {
        self.presentingPopin().dismissCurrentPopinController(animated: true, completion: {
        })
    }
    
    @IBAction func btnOKClicked(sender: UIButton?) {
        self.presentingPopin().dismissCurrentPopinController(animated: true, completion: {
            self.btnOKClicked?(self.selected)
        })
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 0.0
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.items.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if (style == .standard) {
            return DialogSelectCell.rowHeight()
        }
        else
        if (style == .image) {
            return DialogSelectImageCell.rowHeight()
        }
        return 0.0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if (style == .standard) {
            let cell = tableView.dequeueReusableCell(withIdentifier: "DialogSelectCell", for: indexPath) as! DialogSelectCell
            cell.labelName.text = self.items[ indexPath.row ]
            if (indexPath.row == self.selected) {
                cell.ivCheck.image = UIImage(named: "ic_checked.png")
            }
            else {
                cell.ivCheck.image = nil
            }
            return cell
        }
        else
        if (style == .image) {
            let cell = tableView.dequeueReusableCell(withIdentifier: "DialogSelectImageCell", for: indexPath) as! DialogSelectImageCell
            cell.iv.image = UIImage(named: self.images![ indexPath.row ])
            cell.labelName.text = self.items[ indexPath.row ]
            if (indexPath.row == self.selected) {
                cell.ivCheck.image = UIImage(named: "ic_checked.png")
            }
            else {
                cell.ivCheck.image = nil
            }
            return cell
        }
        return UITableViewCell()
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.selected = indexPath.row
        tableView.deselectRow(at: indexPath as IndexPath, animated: true)
        tableView.reloadData()
    }
}
