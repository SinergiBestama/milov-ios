import UIKit

class DialogSelectImageCell: UITableViewCell {
    @IBOutlet weak var iv: UIImageView!
    @IBOutlet weak var labelName: UILabel!
    @IBOutlet weak var ivCheck: UIImageView!
    static func rowHeight () -> CGFloat {
        return 50.0
    }
}
